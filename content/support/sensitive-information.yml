---
title: Handling Sensitive Information With GitLab Support
description: Some common questions and answers about how customers and GitLab support should handle sensitive information
support-hero:
  data:
    title: Handling Sensitive Information With GitLab Support
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'What is sensitive information?'
        href: '#what-is-sensitive-information'                               
      - text: "Where do I need to be careful about divulging sensitive information in interacting with GitLab Support?"
        href: "#where-do-i-need-to-be-careful-about-divulging-sensitive-information-in-interacting-with-gitlab-support"
      - text: "How do I scrub sensitive information?"
        href: "#how-do-i-scrub-sensitive-information"
      - text: "What do I do if I suspect that I have divulged sensitive information in a Support Ticket?"
        href: "#what-do-i-do-if-i-suspect-that-i-have-divulged-sensitive-information-in-a-support-ticket"                                          
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy
  data:
    block:
    - subtitle:
        text: What is sensitive information?
        id: what-is-sensitive-information
      text: |
        <p>Sensitive information is any information that your organization deems sensitive. Commonly:</p>
        <ul>
          <li>credentials</li>
          <li>passwords</li>
          <li>tokens</li>
          <li>
            keys
          </li>
          <li>secrets</li>
        </ul>
        <p>
          These would all be generally be considered sensitive, but your organization likely has its own data classification policies. 
          Your security team might also consider hostnames, IP addresses or other environmental information to be sensitive.
        </p>
    - subtitle:
        text: Where do I need to be careful about divulging sensitive information in interacting with GitLab Support?
        id: where-do-i-need-to-be-careful-about-divulging-sensitive-information-in-interacting-with-gitlab-support
      text: |
        <p>Depending on your configuration, this type of sensitive information can occasionally be in GitLab's configuration files and its components.</p>
        <br />
        <p>Specifically (but not exhaustively):</p>
        <ul>
          <li><code>gitlab.rb</code> for the GitLab application</li>
          <li><code>config.toml</code> for GitLab Runners</li>
          <li><code>.gitlab-ci.yml</code> for GitLab projects</li>
        </ul>
        <p>All <em>could</em> contain information your organization might consider sensitive.</p>
    - subtitle:
        text: How do I scrub sensitive information? 
        id: how-do-i-scrub-sensitive-information
      text: |
        <p>Please do not submit or share files without verifying that the contents have been scrubbed of secrets and private information.</p>
        <br />
        <p>If you or your company/organization is not comfortable sharing it, do not submit it - please.</p>
        <br />
        <p>
          To sanitize a <code>gitlab.rb</code> file, review and redact secrets manually, or use a command line utility to automate this process, 
          for example <a href="https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer">our <code>gitlab.rb</code> sanitizer tool</a>.
        </p>
        <br />
        <p>You can also scrub sensitive data using the grep command line utility.</p>
        <br />
        <blockquote>
          <code>grep -Ev "password|_key|token|secret|app_id|bind_dn|^$|^\s*#" \</code>
          <br />
          <code>/etc/gitlab/gitlab.rb > /tmp/_clean.gitlab.rb</code>
        </blockquote>
        <br />
        <p>In addition to configuration files, <a href="https://docs.gitlab.com/ee/administration/logs.html">log files</a> and traces may include details like internal IP addresses and URLs.</p>
        <br />
        <p>
          To redact details in certain log files, we suggest replacing the sensitive information with placeholder text. 
          There are a number of ways to quickly do a find and replace operation, including common command line utilities like <code>sed</code>:
        </p>
        <br />
        <blockquote>
          <code>sed -i 's/secret-project/project1/g' attachment.log</code>
          <br />
          <code>sed -i 's/private.com/app1.com/g' attachment.log</code>
          <br />
          <code>sed -i 's/10.10.0.2/ip1/g' attachment.log</code>
        </blockquote>
    - subtitle:
        text: What do I do if I suspect that I have divulged sensitive information in a Support Ticket?
        id: what-do-i-do-if-i-suspect-that-i-have-divulged-sensitive-information-in-a-support-ticket
      text: |
        <p>
          If secrets were accidentally shared or attached to a Support ticket, please notify GitLab Support 
          immediately to ensure this data is redacted and deleted.
        </p>
        <br />
        <p>
          Conversely, if a Support Engineer suspects that secrets were accidentally submitted to a Support ticket, 
          we will bring this to your attention and take action to <a href="/handbook/support/workflows/working-on-tickets.html#removing-information-from-tickets">remove any sensitive information</a>.
        </p>
